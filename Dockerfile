FROM caddy:2.8.4
WORKDIR /srv
COPY ./public/ . 

COPY docker-bootstrap.sh /srv
RUN chmod 755 /srv/docker-bootstrap.sh
RUN chmod +x /srv/docker-bootstrap.sh

#RUN sh ./docker-bootstrap.sh

ENTRYPOINT [ "sh", "/srv/docker-bootstrap.sh" ]
CMD [ "caddy", "file-server", "--access-log", "--listen", "0.0.0.0:80", "--root", "/srv" ]
