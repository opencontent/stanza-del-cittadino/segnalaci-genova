#!/bin/bash

if [[ -n PUBLIC_URL ]]; then
  echo "[info] set base url to $PUBLIC_URL"
  sed -i "s,%PUBLIC_URL%,${PUBLIC_URL},g" /srv/assets/script.js
  sed -i "s,%PUBLIC_URL%,${PUBLIC_URL},g" /srv/*.html
fi

if [[ -n SEQUENCIAL_ID_PROVIDER_URL ]]; then
  echo "[info] set base url to $SEQUENCIAL_ID_PROVIDER_URL"
  sed -i "s,%SEQUENCIAL_ID_PROVIDER_URL%,${SEQUENCIAL_ID_PROVIDER_URL},g" /srv/assets/script.js
  sed -i "s,%SEQUENCIAL_ID_PROVIDER_URL%,${SEQUENCIAL_ID_PROVIDER_URL},g" /srv/*.html
fi

exec "$@"
