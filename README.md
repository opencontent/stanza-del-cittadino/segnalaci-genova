# servizio segnalazioni del Comune di Genova

Pagine statiche per il servizio "Segnalaci" del Comune di Genova.

Sulle pagine statiche è inserito il [widget delle segnalazioni](https://gitlab.com/opencity-labs/area-personale/widget-segnalazioni) che costituisce il servizio vero e proprio.

