let translations = {};

document.addEventListener('DOMContentLoaded', () => {
  let langText = document.documentElement.lang.toUpperCase();
    if('i18next' in localStorage){
      const langValue = localStorage['i18next'] ? localStorage['i18next'].split('-')[0] : 'it';
      setLocale(langValue)
      langText = document.querySelector(`[data-value=${langValue}]`).innerHTML;
      document.documentElement.setAttribute("lang", langValue);

    } else {
        document.documentElement.setAttribute("lang", langText);
      setLocale(document.documentElement.lang);
    }
    document.getElementById('localization-selected').innerHTML = langText;
    setDropdownLanguage();
});


let previousUrl = '';
const observer = new MutationObserver(function(mutations) {
    if (location.href !== previousUrl) {
        previousUrl = location.href;
        // Show satisfy widget in thankyou page and detail inefficiency
        if((/thankyou/).test(location.href)){
            addCode("68bfa89a-d884-4022-8a42-af38f44f2c04")
        }else if((/(#\/segnalazioni\/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12})/).test(location.href)){
            addCode("9b74942a-e0bd-4927-a335-c85987e9a92d")
        }else{
            //console.log(`removed URL changed to ${location.href}`);
            removeElement("satisfy")
        }
        //console.log(`URL changed to ${location.href}`);
    }
});
const config = {subtree: true, childList: true};
observer.observe(document, config);


function addCode(id) {
    removeElement("satisfy")
    document.getElementById("satisfy").innerHTML = `
<div class="container">
        <div class="row d-flex justify-content-center bg-primary">
            <div class="col-12 col-lg-6 p-lg-0 px-3">
        <app-widget data-entrypoints="${id}"></app-widget>
    </div>
    </div></div>`;
}

function removeElement(idElem) {
    let element = document.getElementById(idElem)
    if(element){element.innerHTML = ''}
}

const setDropdownLanguage = () => {
  document.querySelectorAll('.localization-option').forEach((element) => {
    element.onclick = (e) => {
        const langValue = e.target.getAttribute('data-value').toLowerCase();
        localStorage['i18next'] = langValue;
        document.documentElement.setAttribute("lang", langValue);
        document.cookie = `i18next=${langValue}`;
        location.reload()
    };
  });
}

const setLocale = async (newLocale) => {
  translations = await fetchTranslations(newLocale);
  translatePage();
};


const fetchTranslations = async (newLocale) => {
  const response = await fetch(`%PUBLIC_URL%/assets/lang/${newLocale}.json`);
  if (!response.ok) {
    console.log(`Could not fetch translations for locale ${newLocale}`);
  }
  return await response.json();
};

const translatePage = () => {
    document.querySelectorAll('[localization-key]').forEach((element) => {
        let key = element.getAttribute('localization-key');
        let translation = translations[key];
        element.innerHTML = translation;
    });
};
